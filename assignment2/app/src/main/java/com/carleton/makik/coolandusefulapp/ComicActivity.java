package com.carleton.makik.coolandusefulapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.widget.TextView;
import android.widget.Toast;


public class ComicActivity extends Activity implements View.OnTouchListener {

    private GestureDetector gestureDetector;

    private final int COMIC_LENGTH = 4;
    private int currentPanel = 1;
    private ImageView[] images;
    private TextView altText;
    private boolean isViewingAltText,isViewingFullComic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic);

        // Set up the system for detecting swipes
        gestureDetector = new GestureDetector(this, new GestureListener());

        // Set up the array of comic panels
        images = new ImageView[COMIC_LENGTH+1];

        images[0] = (ImageView)findViewById(R.id.comic_image);
        images[0].setImageResource(R.drawable.fundamental_forces);

        images[1] = (ImageView)findViewById(R.id.comic_image_1);
        images[1].setImageResource(R.drawable.fundamental_forces_1);

        images[2] = (ImageView)findViewById(R.id.comic_image_2);
        images[2].setImageResource(R.drawable.fundamental_forces_2);

        images[3] = (ImageView)findViewById(R.id.comic_image_3);
        images[3].setImageResource(R.drawable.fundamental_forces_3);

        images[4] = (ImageView)findViewById(R.id.comic_image_4);
        images[4].setImageResource(R.drawable.fundamental_forces_4);

        // Give them this listener and hide them
        for(ImageView image : images){
            image.setOnTouchListener(this);
            image.setVisibility(View.GONE);
        }
        // Display the first panel
        images[currentPanel].setVisibility(View.VISIBLE);

        // Hide the altText
        altText = (TextView)findViewById(R.id.comic_alt_text);
        altText.setOnTouchListener(this);
        altText.setVisibility(View.GONE);

        isViewingAltText = false;
        isViewingFullComic = false;

        // Helpful hints
       Toast.makeText(this, R.string.comic_help_text, Toast.LENGTH_SHORT).show();
    }

    // Helper for transitioning between two views in this activity
    private void transitionViews(View current, Animation out, View next, Animation in){

        current.setVisibility(View.GONE);

        current.startAnimation(out);
        next.startAnimation(in);

        next.setVisibility(View.VISIBLE);
    }

    // Go to a certain panel in the comic with the given in and out animations
    private void goToPanel(int targetPanel, Animation out, Animation in) {

        int tempCurrentPanel = (isViewingFullComic) ? 0 : currentPanel;

        this.transitionViews(images[tempCurrentPanel],out,images[targetPanel],in);

        if(targetPanel != 0){
            currentPanel = targetPanel;
        }

        isViewingFullComic = targetPanel == 0;
    }

    // Go to the next panel
    public void nextPanel(){

        int panel = currentPanel + 1;

        if(panel>COMIC_LENGTH){
            return;
        }

        goToPanel(panel,
                AnimationUtils.loadAnimation(this, R.anim.slide_out_to_left),
                AnimationUtils.loadAnimation(this, R.anim.slide_in_from_right));
    }

    // Go to the previous panel
    public void prevPanel(){

        int panel = currentPanel - 1;

        if(panel<=0){
            return;
        }

        goToPanel(panel,
                AnimationUtils.loadAnimation(this, R.anim.slide_out_to_right),
                AnimationUtils.loadAnimation(this, R.anim.slide_in_from_left));

    }

    // Hide or display the full comic strip
    public void toggleFullComic(){
        if(!isViewingFullComic) {
            this.goToPanel(0,
                    AnimationUtils.loadAnimation(this, R.anim.slide_out_to_top),
                    AnimationUtils.loadAnimation(this, R.anim.slide_in_from_bottom));
        }else{
            this.goToPanel(currentPanel,
                    AnimationUtils.loadAnimation(this, R.anim.slide_out_to_bottom),
                    AnimationUtils.loadAnimation(this, R.anim.slide_in_from_top));
        }
    }

    // Hide or display the alt text
    public void toggleAltText(){
        if(!isViewingAltText){
            this.transitionViews(images[currentPanel],AnimationUtils.loadAnimation(this, R.anim.slide_out_to_bottom),
                    altText,AnimationUtils.loadAnimation(this, R.anim.slide_in_from_top));
        }else{
            this.transitionViews(altText,AnimationUtils.loadAnimation(this, R.anim.slide_out_to_top),
                    images[currentPanel],AnimationUtils.loadAnimation(this, R.anim.slide_in_from_bottom));
        }
        isViewingAltText = !isViewingAltText;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_comic, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        this.finishWithComic();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finishWithComic();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void finishWithComic() {
        this.finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_bottom);
    }

    // Listen for touches and alert the gesture thing
    public boolean onTouch(View v, MotionEvent e){
        return gestureDetector.onTouchEvent(e);
    }

    // This is the structure stackoverflow recommended
    private final class GestureListener extends SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        onSwipeRight();
                    } else {
                        onSwipeLeft();
                    }
                }
            }else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                if (diffY > 0) {
                    onSwipeBottom();
                } else {
                    onSwipeTop();
                }
            }
            return true;
        }
    }

    // Handle each different swipe direction

    private void onSwipeRight() {
        if(!(isViewingAltText||isViewingFullComic)){
            this.prevPanel();
        }
    }

    private void onSwipeLeft() {
        if(!(isViewingAltText||isViewingFullComic)){
            this.nextPanel();
        }
    }

    private void onSwipeTop() {
        if(isViewingAltText) {
            this.toggleAltText();
        }else if(!isViewingFullComic){
            this.toggleFullComic();
        }
    }

    private void onSwipeBottom() {
        if(isViewingFullComic) {
            this.toggleFullComic();
        }else if(!isViewingAltText){
            this.toggleAltText();
        }
    }
}
