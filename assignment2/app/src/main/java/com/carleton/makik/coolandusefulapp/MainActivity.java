package com.carleton.makik.coolandusefulapp;

import android.annotation.TargetApi;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends ListActivity implements View.OnClickListener{
    private final static int MESSAGE_TYPE_COLOR_0=0;
    private final static int MESSAGE_TYPE_COLOR_1=1;
    private final static int MESSAGE_TYPE_COLOR_2=2;
    private static final int COLOR_TRANSITION_DURATION = 5000;


    private TextView aboutScreen;
    private ListView listView;
    private Button button;

    private boolean isStopped = true;
    private BackGroundColorHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize the list and its adapter
        ArrayList<String> listItems = new ArrayList<>();
        listItems.add(getString(R.string.action_title_timer));
        listItems.add(getString(R.string.action_title_comic));
        listItems.add(getString(R.string.action_title_quiz));

        ListAdapter adapter = new ListAdapter(this, R.layout.list_cell, listItems);
        listView = (ListView)this.findViewById(android.R.id.list);
        listView.setAdapter(adapter);

        // Make the about screen invisible
        aboutScreen = (TextView)this.findViewById(R.id.about_screen);
        aboutScreen.setVisibility(View.GONE);

        // Make the button invisible and add its listener
        button = (Button)this.findViewById(R.id.about_ok_button);
        button.setOnClickListener(this);
        button.setVisibility(View.GONE);
    }

    // Start the background color magic every time this activity gets focus
    @Override
    protected void onStart(){
        super.onStart();
        this.startBackgroundColorThread();
    }

    // Stop the background color magic every time this activity loses focus
    @Override
    protected void onStop(){
        super.onStop();
        isStopped = true;
    }

    // This starts the background color thread
    private void startBackgroundColorThread(){
        handler = new BackGroundColorHandler();
        isStopped = false;
        Thread backgroundColorThread = new Thread(new BackgroundColorRunnable());
        backgroundColorThread.setDaemon(true);
        backgroundColorThread.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            this.activateAboutScreen();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Transition to different activities depending on the item selected
    @Override
    public void onListItemClick(ListView parent, View v, int position, long id) {
        TextView textView = (TextView)v.findViewById(R.id.listCellTextView);
        if (textView != null) {
            String str = textView.getText().toString();

            if(str.equals(getString(R.string.action_title_timer))){
                Intent intent = new Intent(this, TimerActivity.class);
                startActivity(intent);
            }else if(str.equals(getString(R.string.action_title_comic))){
                Intent intent = new Intent(this, ComicActivity.class);
                startActivity(intent);
            }else if(str.equals(getString(R.string.action_title_quiz))){
                Intent intent = new Intent(this, QuizActivity.class);
                startActivity(intent);
            }
            overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_left);
        }
    }

    // Hide the list of activities and display the about text and button
    private void activateAboutScreen(){
        listView.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
        aboutScreen.setVisibility(View.VISIBLE);
    }

    // Return to normal view
    @Override
    public void onClick(View view) {
        listView.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
        aboutScreen.setVisibility(View.GONE);
    }

    // Takes updates to activate color transitions
    private class BackGroundColorHandler extends Handler{

        // Android Studio yelled at me until I added this
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        public void handleMessage(Message message) {

            // Start the appropriate transition sequence
            switch (message.what) {
                case MESSAGE_TYPE_COLOR_0:
                    listView.setBackground(getDrawable(R.drawable.color_change_1));
                    break;
                case MESSAGE_TYPE_COLOR_1:
                    listView.setBackground(getDrawable(R.drawable.color_change_2));
                    break;
                case MESSAGE_TYPE_COLOR_2:
                    listView.setBackground(getDrawable(R.drawable.color_change_3));
                    break;
            }

            TransitionDrawable transition = (TransitionDrawable) listView.getBackground();
            transition.startTransition(COLOR_TRANSITION_DURATION);
        }

    }

    private class BackgroundColorRunnable implements Runnable{
        public void run() {
            // Start with one of the three random transitions
            int currentTransition = (int)(Math.random()*3);
            // Stop the thread by flagging with isStopped
            while (!isStopped) {
                long time = System.currentTimeMillis();

                // Tell the handler the next transition is due to be started
                Message message = new Message();
                message.what = currentTransition;
                handler.sendMessage(message);

                currentTransition = (currentTransition+1)%3;

                // Sit in this loop until the previously ordered transition
                // has finished. Check for whether this should stop, too
                while(System.currentTimeMillis() - time < COLOR_TRANSITION_DURATION){
                    if(isStopped) break;
                }
            }
        }
    }
}
