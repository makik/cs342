package com.carleton.makik.coolandusefulapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.view.View.OnClickListener;


public class QuizActivity extends Activity {


    private int currentQuestion;
    private String[] questions;
    private String[] answers;
    private String[] hints;
    private String[] moreInfo;

    private Button button1;
    private Button button2;
    private TextView questionTextView;
    private TextView answerTextView;
    private EditText answerText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        init();
    }


    public void init() {
        //all the information displayed in the quiz
        questions = new String[]{"Are you ready to start a cool and awesome quiz?",
                "What was Harry Potter's mother's first name?",
                "Which Harry Potter word is now in the Oxford Dictionary?",
                "Who was the second oldest Weasley son?",
                "What is the 'disarming' spell?",
                "What is the wizard prison called?",
                "What is the name of Dumbledore's phoenix?",
                "Is this app cool and useful?"};
        answers = new String[]{"Yes", "Lily", "Muggle", "Charlie", "Expelliarmus", "Azkaban", "Fawkes",
                "Yes"};
        hints = new String[]{"Hint: I actually think you are!",
                "Hint: It's also the name of a flower!",
                "Hint: You are one of these.",
                "Hint: Someone of this same name also got to visit a chocolate factory in another popular novel.",
                "Hint: If you forget this spell, you might get 'expelled'... if you know what I mean!",
                "Hint: This is where Serius Black was imprisoned!",
                "Hint: This phoenix saved Harry Potter in the chamber of secrets!",
                "Hint: The correct answer is 'yes'"
        };
        moreInfo = new String[]{"Yay!",
                "Lily was a Muggle-born witch. Voldemort killed her.",
                "In 2003, the OED added \"muggle,\" which the dictionary defines as \"a person who possesses no magical powers ... a person who lacks a particular skill or skills, or who is regarded as inferior in some way.\"",
                "Charlie was a cool dude! He liked dragons.",
                "Harry Potter used Expelliarmus to survive! Pretty cool.",
                "This is not a good place to go.",
                "Rise like a phoenix\n" +
                        "Out of the ashes\n" +
                        "Seeking rather than vengeance\n" +
                        "Retribution\n" +
                        "You were warned\n" +
                        "Once I’m transformed\n" +
                        "Once I’m reborn",
                "Great job! You have finished this cool and useful quiz! But you can just hit 'next question' to start again!"
        };

        currentQuestion = -1;

        //check answer button
        Button button1 = (Button) this.findViewById(R.id.button1);
        //next question button
        Button button2 = (Button) this.findViewById(R.id.button2);


        questionTextView = (TextView) this.findViewById(R.id.questionTextView);
        answerText = (EditText) findViewById(R.id.answerText);
        answerTextView = (TextView) this.findViewById(R.id.answerTextView);

        showQuestion();

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer();
            }
        });
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showQuestion();
            }
        });
    }

    public void showQuestion() {
        currentQuestion++;
        if (currentQuestion == questions.length) {
            currentQuestion = 0;
        }
        questionTextView.setText(questions[currentQuestion]);
        answerText.setText("");
        answerTextView.setText("");
    }

    public boolean isCorrect(String answer) {
        return (answer.equalsIgnoreCase(answers[currentQuestion]));
    }

    public void checkAnswer() {
        String answer = answerText.getText().toString();
        if (isCorrect(answer)) {
            answerTextView.setText(moreInfo[currentQuestion]);
        } else {
            answerTextView.setText(hints[currentQuestion]);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        this.finishWithQuiz();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finishWithQuiz();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void finishWithQuiz() {

        this.finish();
        //overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }
}

