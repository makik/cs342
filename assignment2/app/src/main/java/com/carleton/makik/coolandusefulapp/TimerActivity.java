package com.carleton.makik.coolandusefulapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class TimerActivity extends Activity implements View.OnClickListener{

    private TimerHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        // The TextView that holds the current time (defaults to 00:00:00)
        TextView timerText = (TextView)this.findViewById(R.id.timer_text);

        // The handler is the 'model' part of VMC, initialize and activate it
        handler = TimerHandler.getTimerHandler();
        handler.activate(timerText);

        // Set up the buttons for the timer
        Button button = (Button)this.findViewById(R.id.timer_button);
        button.setOnClickListener(this);
        if(handler.isTimerRunning()) button.setText(R.string.timer_stop_button_label);

        button = (Button)this.findViewById(R.id.timer_reset_button);
        button.setOnClickListener(this);
        if(handler.isTimerRunning() || timerText.getText().equals(getString(R.string.timer_start_time)))
            button.setVisibility(View.INVISIBLE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_timer, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        this.finishWithTimer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finishWithTimer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void finishWithTimer() {

        handler.deactivate();
        this.finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_bottom);
    }

    @Override
    public void onClick(View view) {
        // This is the button clicked
        Button button = (Button)view;
        // This is the reset button
        Button resetButton = (Button)this.findViewById(R.id.timer_reset_button);

        // If the start button is clicked, start the timer
        // If the stop button is clicked, stop the timer
        // If the reset button is clicked, reset the timer
        if(button.getText().equals(getString(R.string.timer_start_button_label))){
            handler.startTimer();
            button.setText(R.string.timer_stop_button_label);
            resetButton.setVisibility(View.INVISIBLE);
        }else if(button.getText().equals(getString(R.string.timer_stop_button_label))){
            handler.stopTimer();
            button.setText(R.string.timer_start_button_label);
            resetButton.setVisibility(View.VISIBLE);
        }else{
            handler.resetTimer();
            resetButton.setVisibility(View.INVISIBLE);
        }
    }
}
