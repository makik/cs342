package com.carleton.makik.coolandusefulapp;

import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

public class TimerHandler extends Handler {
    protected final static int MESSAGE_TYPE_UPDATE = 1;

    // Is the TimerActivity currently on screen
    private boolean isActiveView = false;
    // Is the timer running
    private boolean isStopped = true;
    // The two digit strings of text that make up each section of the timer
    private String[] timeStrings;
    // The TextView object to update with the appropriate time
    private TextView timerText;

    // TimerHandler is a singleton, so make sure only one ever gets created
    private static TimerHandler handler;

    // Private constructor so no one tries to make their own TimerHandler
    private TimerHandler(){}

    // When something needs a timerHandler, this is what it should call
    public static TimerHandler getTimerHandler(){
        if(handler==null){
            handler = new TimerHandler();
        }
        return handler;
    }

    // The TimerActivity is active and it passes in the textView to be updated
    public void activate(TextView textView){
        timerText = textView;
        isActiveView = true;
        updateTimerText();
    }

    // The TimerActivity is no longer visible
    public void deactivate(){
        timerText = null;
        isActiveView = false;
    }

    @Override
    public void handleMessage(Message message){
        switch (message.what){
            case MESSAGE_TYPE_UPDATE:
                // Pull info from the text view if nothing is stored in timeStrings
                if(isActiveView && timeStrings==null) {
                    String time = (String) timerText.getText();
                    timeStrings = time.split(":");
                }
                // Update the timer using basic math made complicated by Java
                if(timeStrings[2].equals("99")){
                    timeStrings[2] = "00";
                    if(timeStrings[1].equals("59")){
                        timeStrings[1] = "00";
                        if(timeStrings[0].equals("59")){
                            timeStrings[0] = "00";
                        }else{
                            int asInt = Integer.parseInt(timeStrings[0])+1;
                            if(asInt < 10) {
                                timeStrings[0] = "0" + asInt;
                            }else{
                                timeStrings[0] = asInt + "";
                            }
                        }
                    }else{
                        int asInt = Integer.parseInt(timeStrings[1])+1;
                        if(asInt < 10) {
                            timeStrings[1] = "0" + asInt;
                        }else{
                            timeStrings[1] = asInt + "";
                        }
                    }
                }else{
                    int asInt = Integer.parseInt(timeStrings[2])+1;
                    if(asInt < 10) {
                        timeStrings[2] = "0" + asInt;
                    }else{
                        timeStrings[2] = asInt + "";
                    }
                }
                updateTimerText();
                break;
        }
    }

    // Create the thread for the timer and run it
    public void startTimer(){
        isStopped = false;
        Thread timerThread = new Thread(new TimerRunnable());
        timerThread.setDaemon(true);
        timerThread.start();
    }

    // Flag the timer thread to stop
    public void stopTimer(){
        isStopped = true;
    }

    // Resets the timer to start time. Will stop the timer if it was running
    public void resetTimer(){
        timeStrings = null;
        isStopped = true;
        updateTimerText();
    }

    // Put the timeStrings into the text view. Default to start time
    public void updateTimerText() {
        if(timeStrings == null){
            timerText.setText(R.string.timer_start_time);
        }else if(isActiveView) {
            String time = timeStrings[0] + ":" + timeStrings[1] + ":" + timeStrings[2];
            timerText.setText(time);
        }
    }

    // Is the timer thread running
    public boolean isTimerRunning() {
        return !isStopped;
    }

    private class TimerRunnable implements Runnable{
        public void run() {
            long time;
            // Stop the thread by flagging with isStopped
            while (!isStopped) {
                // Send a message to update the timer
                Message message = new Message();
                message.what = MESSAGE_TYPE_UPDATE;
                TimerHandler.getTimerHandler().sendMessage(message);

                // Sit in this empty while loop until the appropriate
                // amount of time has passed (currently 0.01 seconds)
                time = System.nanoTime();
                while(System.nanoTime() - time < 10000000);
            }
        }
    }
}