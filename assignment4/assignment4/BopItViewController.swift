//
//  BopItViewController.swift
//  assignment4
//
//  Created by mobiledev on 4/21/15.
//  Copyright (c) 2015 makik. All rights reserved.
//

import UIKit

class BopItViewController: UIViewController {
    
    let actions: [String] = ["Tap it!","Left!","Right!","Up!","Down!"]
    
    @IBOutlet weak var instruction: UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func chooseRandomInstruction(){
        var prev = instruction!.text
        var next = Int(arc4random_uniform(UInt32(actions.count)))
        if(actions[next] == prev){
            next = (next+1)%actions.count
        }
        instruction!.text = actions[next]
    }
    
    
    @IBAction func handleTap(recognizer:UITapGestureRecognizer) {
        if(instruction!.text==actions[0]){
            chooseRandomInstruction()
        }
    }
    
    @IBAction func handleSwipeLeft(recognizer:UISwipeGestureRecognizer) {
        if(instruction!.text==actions[1]){
            chooseRandomInstruction()
        }
    }
    
    @IBAction func handleSwipeRight(recognizer:UISwipeGestureRecognizer) {
        if(instruction!.text==actions[2]){
            chooseRandomInstruction()
        }
    }
    
    @IBAction func handleSwipeUp(recognizer:UISwipeGestureRecognizer) {
        if(instruction!.text==actions[3]){
            chooseRandomInstruction()
        }
    }
    
    @IBAction func handleSwipeDown(recognizer:UISwipeGestureRecognizer) {
        if(instruction!.text==actions[4]){
            chooseRandomInstruction()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
