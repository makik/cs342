//
//  ColorViewController.swift
//  assignment4
//
//  Created by mobiledev on 4/19/15.
//  Copyright (c) 2015 makik. All rights reserved.
//

import UIKit

class ColorViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    
    var colors = ["White","Red","Orange","Yellow","Green","Blue","Purple","Gray","Slate"]

    
    @IBOutlet weak var redSlider: UISlider?
    @IBOutlet weak var greenSlider: UISlider?
    @IBOutlet weak var blueSlider: UISlider?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colors.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return colors[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.updateBackgroundColor(colors[pickerView.selectedRowInComponent(component)])
    }
    
    func updateBackgroundColor(newColor: NSString) {
        var newColorRGB = getColorDictionary()[newColor]
        self.view.backgroundColor = newColorRGB
        
        var colorString = newColorRGB!.description
        let newString = colorString.stringByReplacingOccurrencesOfString("UIDeviceRGBColorSpace", withString: "")
        let myWordList = newString.wordList
        let red = (myWordList[1] as NSString).floatValue
        let green = (myWordList[2] as NSString).floatValue
        let blue = (myWordList[3] as NSString).floatValue
        
        redSlider!.value = red
        blueSlider!.value = blue
        greenSlider!.value = green
        
    }
    
    func getColorDictionary() -> [String: UIColor]{
        var colorDictionary : [String:UIColor] = [:]
        colorDictionary["White"]=UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        colorDictionary["Red"] = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)
        colorDictionary["Orange"]=UIColor(red: 1.0, green: 0.6, blue: 0.2, alpha: 1.0)
        colorDictionary["Yellow"]=UIColor(red: 1.0, green: 1.0, blue: 0.0, alpha: 1.0)
        colorDictionary["Green"]=UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 1.0)
        colorDictionary["Blue"]=UIColor(red: 0.0, green: 0.0, blue: 1.0, alpha: 1.0)
        colorDictionary["Purple"]=UIColor(red: 0.6, green: 0.0, blue: 0.6, alpha: 1.0)
        colorDictionary["Gray"]=UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0)
        colorDictionary["Slate"]=UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1.0)
        return colorDictionary
    }
    
    @IBAction func sliderValueChanged(sender: UISlider){
        self.view.backgroundColor = UIColor(red: CGFloat(redSlider!.value), green: CGFloat(greenSlider!.value), blue: CGFloat(blueSlider!.value), alpha: 1.0)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension String {
    var wordList:[String] {
        return "".join(componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: ","))).componentsSeparatedByString(" ")
    }
}