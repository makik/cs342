//
//  ComicViewController.swift
//  assignment4
//
//  Created by mobiledev on 4/20/15.
//  Copyright (c) 2015 makik. All rights reserved.
//

import UIKit

class ComicViewController: UIViewController {
    
    @IBOutlet weak var comicSegments: UISegmentedControl?
    @IBOutlet weak var comicButton: UIButton?
    var altText: String?
    
    let altTextArray: [String] = ["I honestly didn't think you could even USE emoji in variable names. Or that there were so many different crying ones.",
        "Do you think you could actually clean the living room at some point, though?",
        "Here at CompanyName.website, our three main stengths are our web-facing chairs, our huge collection of white papers, and the fact that we physically cannot die.",
        "\"Of these four forces, there\'s one we don\'t really understand.\" \"Is it the weak force or the strong--\" \"It\'s gravity.\""]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIDevice.currentDevice().setValue(UIInterfaceOrientation.LandscapeLeft.rawValue, forKey: "orientation")
        
        self.altText = self.altTextArray[0]

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func indexChanged(sender:UISegmentedControl)
    {
        var image: UIImage?
        switch comicSegments!.selectedSegmentIndex
        {
        case 0:
            image = UIImage(named: "code_quality")
        case 1:
            image = UIImage(named: "vacuum")
        case 2:
            image = UIImage(named: "meeting")
        case 3:
            image = UIImage(named: "fundamental_forces")
        default:
        break;
        }
        self.altText = self.altTextArray[comicSegments!.selectedSegmentIndex]
        comicButton?.setImage(image, forState: .Normal)
    }
    
    @IBAction func displayAltText(sender:UIButton){
        var alert = UIAlertController(title: "", message: self.altText, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
