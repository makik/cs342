//
//  MainViewController.swift
//  assignment4
//
//  Created by mobiledev on 4/21/15.
//  Copyright (c) 2015 makik. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var optionsTableView: UITableView?

    let cellIdentifier = "OptionsCell"
    let options = ["Comics","Colors","iBopIt"]
    
    let aboutText = "This app has 3 very useful and very interesting uses. The first is a comic viewer, where you can select from multiple XKCD comics and view the alt text by tapping. The second is a color changer featuring both preset and custom colors. The last (but certainly not least) is the reinvention and renewal of the classic \"BopIt\" game only 90s kids will remember."
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
        }
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        cell.textLabel?.text = self.options[indexPath.row]
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return indexPath
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch(indexPath.row){
        case 0:
            performSegueWithIdentifier("Comics", sender: self)
        case 1:
            performSegueWithIdentifier("Colors", sender: self)
        case 2:
            performSegueWithIdentifier("BopIt", sender: self)
        default:
            break
        }
    }
    
    @IBAction func displayAboutText(sender:UIBarButtonItem){
        var alert = UIAlertController(title: "About", message: self.aboutText, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

}
